NVCC =nvcc
CC=gcc

# CUDAFLAGS = -arch=compute_50 # Maxwell (50, 52, 53)
# CUDAFLAGS = -arch=compute_60 # Pascal (60, 61, 62)
# CUDAFLAGS = -arch=compute_70 # Volta (70, 72)
# CUDAFLAGS = -arch=compute_75 # Turing
# CUDAFLAGS = -arch=compute_80 # Ampere

#CUDAFLAGS = -arch=compute_60 -lineinfo -O3 -maxrregcount 24
CUDAFLAGS = -arch=compute_60 -O3

SOURCES = $(wildcard src/*.cu)
OBJS := $(patsubst %.cu,%.o,$(SOURCES))

TARGET = gravity

.DEFAULT: all

all: link

link: ${OBJS}
	${NVCC} ${CUDAFLAGS} -o ${TARGET} ${OBJS}

%.o: %.cu
	${NVCC} ${CUDAFLAGS} -c $< -o $@

clean:
	rm -f src/*.o 	
	rm -f ${TARGET}
