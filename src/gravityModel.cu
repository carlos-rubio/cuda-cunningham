/* 
    CUDA Cunningham - Efficient gravity gradient computation in CUDA enabled GPUs
    Copyright (C) 2024 Carlos Rubio <carlos.rubio@unileon.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "config.h"
#include "macro.h"
#include "gravityModel.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/** Replace char in string */
char* replaceChar(char* str, char find, char replace);

/** Read Stokes coeffcients from the gravity model file (EGM2008) */
void readEarthGravityModel(double* coeffsCnm, double* coeffsSnm, int maxDegree, const char* file){
    FILE * fp;
    char * line = NULL;
    size_t len = 0;    
    char tmp[26];

    fp = fopen(file, "r");
    if (fp == NULL)
        HANDLE_ERROR(IO_ERROR);

    coeffsCnm[0*maxDegree+0] = 1.0;
    coeffsSnm[0*maxDegree+0] = 0.0;
    coeffsCnm[1*maxDegree+0] = 0.0;
    coeffsSnm[1*maxDegree+0] = 0.0;
    coeffsCnm[1*maxDegree+1] = 0.0;
    coeffsSnm[1*maxDegree+1] = 0.0;

    while (getline(&line, &len, fp) != -1) {
        // read degree
        strncpy(tmp, line, 5);
        tmp[5] = 0;
        int n = atoi(tmp);

        // read order
        strncpy(tmp, line+5, 5);
        tmp[5] = 0;
        int m = atoi(tmp);

        // read Cnm
        strncpy(tmp, line+10, 25);
        tmp[25] = 0;
        replaceChar(tmp, 'D','E');        
        double Cnm = atof(tmp);

        // read Snm
        strncpy(tmp, line+35, 25);
        tmp[25] = 0;
        replaceChar(tmp, 'D','E');        
        double Snm = atof(tmp);
    
        // assuming order in the file
        if(n > maxDegree) break;

        // store coefficients
        coeffsCnm[n*maxDegree+m] = Cnm;
        coeffsSnm[n*maxDegree+m] = Snm;
    }

    fclose(fp);
    if (line)
        free(line);    
}

/** Replace char in string */
char* replaceChar(char* str, char find, char replace){
    char *index = strchr(str,find);
    while (index) {
        *index = replace;
        index = strchr(index,find);
    }
    return str;
}