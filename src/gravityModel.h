/* 
    CUDA Cunningham - Efficient gravity gradient computation in CUDA enabled GPUs
    Copyright (C) 2024 Carlos Rubio <carlos.rubio@unileon.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef GRAVITY_MODEL_H
#define GRAVITY_MODEL_H

/** Gravity model data file */
#define GRAVITY_MODEL_FILE "egm08_to2.dat"

/** Earth reference radius (value accoding to the EGM2008 Global Graviational Model) */
static const double AE = 6378136.3;

/** Earth gravitational parameter (value accoding to the EGM2008 Global Graviational Model) */
static const double GM = 3986004.415e8;

/** Read Stokes coeffcients from the gravity model file (EGM2008) */
void readEarthGravityModel(double* coeffsCnm, double* coeffsSnm, int maxDegree, const char* file);

#endif