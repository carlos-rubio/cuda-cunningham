/* 
    CUDA Cunningham - Efficient gravity gradient computation in CUDA enabled GPUs
    Copyright (C) 2024 Carlos Rubio <carlos.rubio@unileon.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <unistd.h>  
#include <stdio.h>
#include <signal.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>
#include <cuda_profiler_api.h>
#include "common.h"
#include "macro.h"
#include "memory.h"
#include "device.h"
#include "middleware.h"
#include "cunningham.h"
#include "gravityModel.h"

/** Register ctrl-C handler */
void registerSignalHandlers(void);

/** Ctrl+C handler */
void signalHandler(int signal);

/** Print usage and exit */
void printUsageAndExit(const char* command);

/** TX and RX message queue */
static int queueRx;
static int queueTx;

/** Main function */
int main(int argc, char **argv) {  
    // autotest executes one preconfigured computation, useful for testing purposes
    bool autotest = false;

    // parameters check
    if(argc == 3){
        if(!strcmp(argv[2], "autotest" )) autotest = true;
        else printUsageAndExit(argv[0]);
    }
    if(argc != 2 && argc != 3) printUsageAndExit(argv[0]);
    
    // manage ctrl+C
    registerSignalHandlers();

    // select and print device info
    int device = selectBestDevice();
    printDevice(device);
    HANDLE_CUDA(cudaSetDevice(device) );  

    // read maximum degree/order
    int degree = atoi(argv[1]);
    if(degree < 2 || degree > MAX_DEGREE){
        printf("Degree should be betweem 2 to %d\n",MAX_DEGREE);
        return EXIT_FAILURE;
    }

    // Alloc memory for the Stokes coefficients
    double* coeffsCnm;
    double* coeffsSnm;
    coeffsCnm = (double*)malloc((degree+1)*(degree+1)*sizeof(double));
    coeffsSnm = (double*)malloc((degree+1)*(degree+1)*sizeof(double));

    // read Earth gravity model from EGM2008 file
    readEarthGravityModel(coeffsCnm, coeffsSnm, degree, GRAVITY_MODEL_FILE);

    // precompute factors
    size_t factorsMemorySize;
    Factors* factors = precomputeFactors(coeffsCnm, coeffsSnm, degree, &factorsMemorySize);

    // copy factors to device memory (can be global constant)
    Factors* factors_device;
    HANDLE_CUDA( cudaMalloc( (void**)&factors_device, factorsMemorySize ) );
    HANDLE_CUDA( cudaMemcpy( factors_device , factors, factorsMemorySize, cudaMemcpyHostToDevice) ); 

    // open communication queues  
    if(!autotest){
        queueTx = openTxQueue();
        queueRx = openRxQueue();
    }

    // request and response buffers
    Message request;
    Message response;

    // allocate response message on device
    Message* response_device;
    HANDLE_CUDA( cudaMalloc( (void**)&response_device, sizeof(Message)) );

    // allocate input data space on device (can be global constant)
    Inputs* inputs_device;
    HANDLE_CUDA( cudaMalloc( (void**)&inputs_device, sizeof(Inputs)) );
    Inputs inputs; // Inputs host

    while(true) {
        // start nvprof profiling
        if(autotest) cudaProfilerStart();

        // receive next request (blocking call)
        if(!autotest){
            int ret = receiveRequest(queueRx, &request);
            
            // exit if received a request with negative size
            if(ret == -1 || request.size < 0) break;
        } else {
            // create a synthetic request (autotest mode)  
            request.size = CONCURRENT_INPUTS;
            for(int i=0;i<request.size;i++){
                request.data[0].x = 2182833.310686714;
                request.data[0].y = -6261684.14221788;
                request.data[0].z = 499049.5374745721;
            }
        }

#ifdef ENABLE_LOG
        // elapsed time measurement
        struct timespec start, end;
        clock_gettime(CLOCK_REALTIME, &start); // start time measurement
#endif

        // convert request to Inputs
        // this conversion is performed at host to minimize kernel size
        convertRequest(&request, &inputs);
        
        // copy Inputs to device
        HANDLE_CUDA(cudaMemcpy( inputs_device, &inputs, sizeof(Inputs), cudaMemcpyHostToDevice ) );

        // call the kernel
        int blocks = request.size;
        int threads = degree+2;
        kernel<<<blocks,threads>>>(degree, factors_device, inputs_device, response_device);

        // copy result from device to host
        HANDLE_CUDA(cudaMemcpy( &response, response_device, sizeof(Message), cudaMemcpyDeviceToHost ) );

        cudaDeviceSynchronize();

#ifdef ENABLE_LOG
        // elapsed time measurement
        clock_gettime(CLOCK_REALTIME, &end);
        double time_spent = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1000000000.0;
#endif
        // set response size
        response.size = request.size;

        // send response
        if(!autotest){        
            sendResponse(queueTx, &response);
        }

        // stop nvprof profiling
        if(autotest) cudaProfilerStop();

#ifdef ENABLE_LOG
        // print elapsed time
        printf("Elapsed: %f ms\n", time_spent*1000.0);

        // print request
        printf("Request size: %d\n", request.size);
        printf("Request first vector: %.17e, %.17e, %.17e\n", request.data[0].x,request.data[0].y,request.data[0].z);

        // print result
        printf("Result size: %d\n", response.size);
        printf("Result first vector: %.17e, %.17e, %.17e\n", response.data[0].x,response.data[0].y,response.data[0].z);
#endif
        if(autotest) break;
    }        
    // free device memory
    cudaFree(factors_device);
    cudaFree(inputs_device);
    cudaFree(response_device);

    // free host memory
    free(factors);
    free(coeffsCnm);
    free(coeffsSnm);

    // close queues
    if(!autotest){    
        closeQueue(queueTx);
        closeQueue(queueRx);
    }

    return EXIT_SUCCESS;
}

/** Register ctrl-C handler */
void registerSignalHandlers(){        
    struct sigaction action;
    action.sa_handler = signalHandler;    
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;    
    sigaction(SIGINT, &action, NULL);
    sigaction(SIGTERM, &action, NULL);
    sigaction(SIGQUIT, &action, NULL);    
}

/** On ctrl+C enqueue an exit request */ 
void signalHandler(int signal){    
    Message request;
    request.size = -1;
    int priority = 0;
    int ret = mq_send(queueRx, (char*)&request, sizeof(Message), priority);
}

/** Print usage and exit */
void printUsageAndExit(const char* command){
    printf("Usage: %s degree {autotest}\n", command);
    exit(EXIT_SUCCESS);
}
