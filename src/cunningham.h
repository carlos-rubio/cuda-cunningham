/* 
    CUDA Cunningham - Efficient gravity gradient computation in CUDA enabled GPUs
    Copyright (C) 2024 Carlos Rubio <carlos.rubio@unileon.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CUNNINGHAM_H
#define CUNNINGHAM_H

#include "config.h"
#include "common.h"
#include "middleware.h"

/** Define the double or simple precision datatype */
#ifdef FLOAT_VERSION
    typedef float float_tp;
#else
    typedef double float_tp;
#endif

/** Kernel input data for one position */
typedef struct {
    float_tp h1;
    float_tp h2;
    float_tp h3;
    float_tp qx;    
    float_tp qy;
    float_tp qz;
    float_tp qr;
} Input;

/** Kernel input data */
typedef struct { 
 Input data[CONCURRENT_INPUTS]; 
} Inputs;

/** Pre-computed factors memory layout */
typedef struct __align__(128) {
  float_tp f0[(MAX_DEGREE+2)];
  float_tp f1[(MAX_DEGREE+2)*(MAX_DEGREE+2)];
  float_tp f2[(MAX_DEGREE+2)*(MAX_DEGREE+2)];
  float_tp f3[(MAX_DEGREE+2)*(MAX_DEGREE+2)];
  float_tp f4[(MAX_DEGREE+2)*(MAX_DEGREE+2)];
  float_tp f5[(MAX_DEGREE+2)*(MAX_DEGREE+2)];
  float_tp f6[(MAX_DEGREE+2)*(MAX_DEGREE+2)];
  float_tp f7[(MAX_DEGREE+2)*(MAX_DEGREE+2)];
  float_tp f8[(MAX_DEGREE+2)*(MAX_DEGREE+2)];
  float_tp f9[(MAX_DEGREE+2)*(MAX_DEGREE+2)];
} Factors;

/** Pre-compute factors */
Factors* precomputeFactors(double* coeffsCnm, double* coeffsSnm, int maxDegree, size_t* size);

/** Convert request message to kernel input */
void convertRequest(Message* request, Inputs* inputs);

/** Kernel function */
__global__ void kernel(const int degree,__restrict__ const Factors* factors,__restrict__  const Inputs* inputs, Message* result);

/** Empty kernel function. Used to test queue mechanism delay */
__global__ void kernelEmpty(const int degree,__restrict__  const Factors* factors,__restrict__  const Inputs* inputs, Message* result);

#endif