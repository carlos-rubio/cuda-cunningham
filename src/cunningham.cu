/* 
    CUDA Cunningham - Efficient gravity gradient computation in CUDA enabled GPUs
    Copyright (C) 2024 Carlos Rubio <carlos.rubio@unileon.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <math.h>
#include "cunningham.h"
#include "gravityModel.h"

/** SQRT(3) constant*/
static const double SQRT3 = 1.73205080756887729;

/** Pre-compute factors */
Factors* precomputeFactors(double* coeffsCnm, double* coeffsSnm, int degree, size_t* memorySize){
    int factorsSize = degree+2;    
    *memorySize = sizeof(Factors);
    Factors* factors = (Factors*)malloc(*memorySize);
    memset(factors, 0, *memorySize);
    
    double scaler = GM / (AE*AE);
    
    for(int n=2;n<=degree;n++){
        for(int m=0;m<=n;m++){
            double Cnm = coeffsCnm[n*degree+m];
            double Snm = coeffsSnm[n*degree+m];
            double k = 1.0;
            if(m == 1) k = 2.0;            
            double tmp1 = sqrt( ( (n + m + 2.0) * (n + m + 1.0) * (2.0 * n + 1.0)  ) / (2.0 * n + 3.0) );
            double tmp2 = sqrt( (k * (2.0 * n + 1.0) * (n - m + 2.0) * (n - m + 1.0)) / (2.0 * n + 3.0) );
            double tmp3 = sqrt( ( (n + m + 1.0) * (2.0 * n + 1.0) ) / ( (n - m + 1.0) * (2.0 * n + 3.0) ) );
            double k2 = (n-m+1);            
            factors->f1[n*factorsSize+m] = -Cnm * (0.5 * tmp1 * scaler); // Eq. 31
            factors->f2[n*factorsSize+m] = -Snm * (0.5 * tmp1 * scaler); // Eq. 32
            factors->f3[n*factorsSize+m] = Cnm * (0.5 * tmp2 * scaler); // Eq. 33
            factors->f4[n*factorsSize+m] = Snm * (0.5 * tmp2 * scaler); // Eq. 34
            factors->f5[n*factorsSize+m] = -Cnm * (k2 * tmp3 * scaler); // Eq. 35
            factors->f6[n*factorsSize+m] = -Snm * (k2 * tmp3 * scaler); // Eq. 36
        }
        double Cn = coeffsCnm[n*degree];
        double tmp4 = sqrt( ( (n + 1.0) * (n + 2.0) * (2.0 * n + 1.0) ) / ( 2.0 * (2.0 * n + 3.0) ) );
        factors->f0[n] = -Cn * (tmp4 * scaler); // Eq. 30
    }

    for(int n=0;n<factorsSize;n++){
        for(int m=0;m<factorsSize;m++){            
            factors->f7[n*factorsSize+m] = (2.0 * m - 1.0) * sqrt((2.0 * n + 1.0) / ((n + m) * (n + m - 1) * (2.0 * n - 1.0))); // Eq. 37
            factors->f8[n*factorsSize+m] = sqrt(((2.0 * n - 1.0) * (2.0 * n + 1.0)) / ((n - m) * (n + m))); // Eq. 38
            factors->f9[n*factorsSize+m] = sqrt(((n + m - 1.0) * (n - m - 1.0) * (2.0 * n + 1.0)) / ((n + m) * (n - m) * (2.0 * n - 3.0))); // Eq. 39
        }
    }
    return factors;
}

/** 
 * Convert request message to kernel input  
 * Performed at host to minimize the kernel size
 */
void convertRequest(Message* request, Inputs* inputs){    
    for(int i=0;i<request->size;i++){
        double x = request->data[i].x;
        double y = request->data[i].y;
        double z = request->data[i].z;
        double r2 = x*x + y*y + z*z;
        double r = sqrt(r2);
        double rInv = 1.0 / r;
        double h1 = AE * rInv; // Eq. 40a               
        double h3 = rInv * h1; // Eq. 40c
        inputs->data[i].h1 = h1;
        inputs->data[i].h2 = SQRT3 * h1; // Eq. 40b
        inputs->data[i].h3 = h3;
        inputs->data[i].qx = h3 * x; // Eq. 41a
        inputs->data[i].qy = h3 * y; // Eq. 41b
        inputs->data[i].qz = h3 * z; // Eq. 41c
        inputs->data[i].qr = h3 * AE; // Eq. 41d
    }
}

/** Empty kernel function. Used to test queue mechanism delay */
__global__ void kernelEmpty(const int degree,__restrict__  const Factors* factors, __restrict__  const Inputs* inputs, Message* result){
}

/** Kernel function */
__global__ void kernel(const int degree,__restrict__  const Factors* factors, __restrict__  const Inputs* inputs, Message* result){
    // number of blocks = input.size
    // number of threads = degree+2

    // Vnm and Wnm last terms (by order, m)
    __shared__ float_tp Vnms[2][MAX_DEGREE+2];
    __shared__ float_tp Wnms[2][MAX_DEGREE+2];

    // xs, ys, and zs accumulators (by order, m)
    __shared__ double xs[MAX_DEGREE+2];
    __shared__ double ys[MAX_DEGREE+2];
    __shared__ double zs[MAX_DEGREE+2];

#ifdef DOUBLE_ACCUMULATORS      
    // xs, ys, and zs accumulators (by degree, n)
    __shared__ double xns[MAX_DEGREE-1];
    __shared__ double yns[MAX_DEGREE-1];
    __shared__ double zns[MAX_DEGREE-1];
#endif

    // one thread for each order, index m
    const int m = threadIdx.x;

    // one block for each input, index block
    const int block = blockIdx.x;

    // copy input to local memory    
    __shared__ Input input;
    if(m == 0) { input = inputs->data[block]; }
    
    // initialize accumulators to zero
    if(m < (MAX_DEGREE+2)){
        xs[m] = 0.0;
        ys[m] = 0.0;
        zs[m] = 0.0;
    }

#ifdef DOUBLE_ACCUMULATORS
    // initialize accumulators to zero
    if(m < (MAX_DEGREE-1)){
        xns[m] = 0.0;
        yns[m] = 0.0;
        zns[m] = 0.0;
    }
#endif

    // used factors vectors size
    const int factorsSize = degree+2; 

    // indexes of the two last Vnm and Wnm vectors
    int indexPrev = 0;
    int indexPrevPrev = 1;

#ifndef DOUBLE_ACCUMULATORS    
    // tmp variable to avoid divergent sums in accumulations
    double tmp;
#endif

    __syncthreads();

    for(int n=0;n<=(degree+1);n++){
        // Global memory index
        int index = n*factorsSize+m; 
        
        float_tp Vnm;
        if (m > n) Vnm = 0.0f;
        else if (n == 0 && m == 0) Vnm = input.h1; // Eq. 42a
        else if (n == 1 && m == 0) Vnm = input.h2 * input.qz; // Eq. 42b
        else if (n == 1 && m == 1) Vnm = input.h2 * input.qx; // Eq. 42c
        else if (n == m) Vnm = factors->f7[index] * (input.qx * Vnms[indexPrev][n - 1] - input.qy * Wnms[indexPrev][n - 1]);  // Eq. 23
        else Vnm = (factors->f8[index] * input.qz) * Vnms[indexPrev][m] - (factors->f9[index] * input.qr) * Vnms[indexPrevPrev][m];  // Eq. 25

        float_tp Wnm;
        if (m > n) Wnm = 0.0f;
        else if (n == 0 && m == 0) Wnm = 0.0f;
        else if (n == 1 && m == 0) Wnm = 0.0f;
        else if (n == 1 && m == 1) Wnm = input.h2 * input.qy; // Eq. 42d
        else if (n == m) Wnm = factors->f7[index] * (input.qx * Wnms[indexPrev][n - 1] + input.qy * Vnms[indexPrev][n - 1]);   // Eq. 24
        else Wnm = (factors->f8[index] * input.qz) * Wnms[indexPrev][m] - (factors->f9[index] * input.qr) * Wnms[indexPrevPrev][m];   // Eq. 26

       __syncthreads();

        // rotate Vnm and Wnm vectors
        indexPrev = !indexPrev;
        indexPrevPrev = !indexPrevPrev;        
    
        // Store last Vnm and Wnm terms
        Vnms[indexPrev][m] = Vnm;
        Wnms[indexPrev][m] = Wnm;

        __syncthreads();
    
#ifdef DOUBLE_ACCUMULATORS
        // gradient accumulation, double summation process
        if(n > 2 && n <=(degree+1) && m <=degree){
          int index = (n-1)*factorsSize+m;

          if (m == 0) xs[m] = factors->f0[(n-1)] * Vnms[indexPrev][1];
          else xs[m] = factors->f1[index] * Vnms[indexPrev][m + 1] + factors->f2[index] * Wnms[indexPrev][m + 1] + factors->f3[index] * Vnms[indexPrev][m - 1] + factors->f4[index] * Wnms[indexPrev][m - 1];
        
          if (m == 0) ys[m] = factors->f0[(n-1)] * Wnms[indexPrev][1];
          else ys[m] = factors->f1[index] * Wnms[indexPrev][m + 1] - factors->f2[index] * Vnms[indexPrev][m + 1] - factors->f3[index] * Wnms[indexPrev][m - 1] + factors->f4[index] * Vnms[indexPrev][m - 1];
        
          zs[m] = factors->f5[index] * Vnms[indexPrev][m] + factors->f6[index] * Wnms[indexPrev][m];
        }  

        __syncthreads();

        // reduction process for row acumulator
        for(int s=1;s<=MAX_DEGREE;s*=2){
            int indexDst = 2 * s * m;
            int indexSrc = indexDst + s;
            if(indexSrc <= MAX_DEGREE){
                xs[indexDst] += xs[indexSrc];
                ys[indexDst] += ys[indexSrc];
                zs[indexDst] += zs[indexSrc];
            }
            __syncthreads();
        }
        // save result of row accumulattion to degree cell
        if(m == 0){        
            xns[n-3] += xs[0];
            yns[n-3] += ys[0];
            zns[n-3] += zs[0];
        }      
#else      
        // gradient accumulation, single summation process
        if(n > 2 && n <=(degree+1) && m <=degree){
          int index = (n-1)*factorsSize+m;

          if (m == 0) tmp = factors->f0[(n-1)] * Vnms[indexPrev][1];
          else tmp = factors->f1[index] * Vnms[indexPrev][m + 1] + factors->f2[index] * Wnms[indexPrev][m + 1] + factors->f3[index] * Vnms[indexPrev][m - 1] + factors->f4[index] * Wnms[indexPrev][m - 1];
          xs[m] += tmp;

          if (m == 0) tmp = factors->f0[(n-1)] * Wnms[indexPrev][1];
          else tmp = factors->f1[index] * Wnms[indexPrev][m + 1] - factors->f2[index] * Vnms[indexPrev][m + 1] - factors->f3[index] * Wnms[indexPrev][m - 1] + factors->f4[index] * Vnms[indexPrev][m - 1];
          ys[m] += tmp;

          zs[m] += factors->f5[index] * Vnms[indexPrev][m] + factors->f6[index] * Wnms[indexPrev][m];
        }                 
#endif       
    } // for degree

__syncthreads();

#ifdef DOUBLE_ACCUMULATORS    
    // gradient accumulation reduction, double summation process
    // xns, yns, and zns reduction
    for(int s=1;s<(MAX_DEGREE-1);s*=2){
        int indexDst = 2 * s * m;
        int indexSrc = indexDst + s;
        if(indexSrc < (MAX_DEGREE-1)){
            xns[indexDst] += xns[indexSrc];
            yns[indexDst] += yns[indexSrc];
            zns[indexDst] += zns[indexSrc];
        }
        __syncthreads();
    }

    // only one thread store the final result
    if(m == 0){
        result->data[block].x = xns[0];
        result->data[block].y = yns[0];
        result->data[block].z = zns[0];
    }
    
#else    
    // gradient accumulation reduction, single summation process
    // xs, ys, and zs reduction
    for(int s=1;s<(MAX_DEGREE+2);s*=2){
        int indexDst = 2 * s * m;
        int indexSrc = indexDst + s;
        if(indexSrc < (MAX_DEGREE+2)){
            xs[indexDst] += xs[indexSrc];
            ys[indexDst] += ys[indexSrc];
            zs[indexDst] += zs[indexSrc];
        }
        __syncthreads();
    }

    // only one thread store the final result
    if(m == 0){
        result->data[block].x = xs[0];
        result->data[block].y = ys[0];
        result->data[block].z = zs[0];
    }   
#endif 
}
