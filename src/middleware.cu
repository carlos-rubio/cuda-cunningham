/* 
    CUDA Cunningham - Efficient gravity gradient computation in CUDA enabled GPUs
    Copyright (C) 2024 Carlos Rubio <carlos.rubio@unileon.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <string.h>  
#include <fcntl.h>
#include <sys/stat.h>
#include "middleware.h"
#include "macro.h"

/** Open response queue */
int openTxQueue(){
    // remove the queue if already exists
    mq_unlink(TX_QUEUE_NAME);

    // set the queue attributes
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = sizeof(Message);
    attr.mq_curmsgs = 0;

    // open or create the TX queue    
    int ret = mq_open(TX_QUEUE_NAME, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR, &attr);
    if(ret == -1) HANDLE_ERROR(IO_ERROR);
    return ret;
}

/** Open request queue */
int openRxQueue(){
    // remove the queue if already exists
    mq_unlink(RX_QUEUE_NAME);

    // set the queue attributes
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = sizeof(Message);
    attr.mq_curmsgs = 0;

    // open or create the RX queue    
    int ret = mq_open(RX_QUEUE_NAME, O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR, &attr);
    if(ret == -1) HANDLE_ERROR(IO_ERROR);
    return ret;
}

/** Close queue */
void closeQueue(mqd_t mqdes){
    mq_close(mqdes);
}

/** Send results */
void sendResponse(int mqd, Message *message){
    unsigned int priority = 0;
    int ret = mq_send(mqd, (char*)message, sizeof(Message), priority);
    if(ret) HANDLE_ERROR(IO_ERROR);    
}

/** Receive next request (blocking method)*/
int receiveRequest(int mqd, Message *message){
    unsigned int priority = 0;
    return mq_receive(mqd, (char *)message, sizeof(Message), &priority);        
}