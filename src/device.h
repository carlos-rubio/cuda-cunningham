/* 
    CUDA Cunningham - Efficient gravity gradient computation in CUDA enabled GPUs
    Copyright (C) 2024 Carlos Rubio <carlos.rubio@unileon.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DEVICE_H
#define DEVICE_H

/*
* prints some info of all detected CUDA GPUs
*/
void printDevices(void);

/**
 * Prints some info of one local CUDA GPUs
 */
void printDevice(int device);

/**
 * Selects the GPU with the max number of multiprocessors
 */
int selectBestDevice();

/**
 * Gets the maximum number of threads per block of one local CUDA GPU
 */
int getMaxThreadsPerBlock(int device);

#endif