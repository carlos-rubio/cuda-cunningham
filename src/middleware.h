/* 
    CUDA Cunningham - Efficient gravity gradient computation in CUDA enabled GPUs
    Copyright (C) 2024 Carlos Rubio <carlos.rubio@unileon.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MIDDLEWARE_H
#define MIDDLEWARE_H

#include <mqueue.h>
#include "config.h"
#include "common.h"

/** Posix queue names */
#define RX_QUEUE_NAME "/cuda-gravity-input"
#define TX_QUEUE_NAME "/cuda-gravity-output"

/** Input and output message */
#pragma pack(1)
typedef struct{ 
 int size;
 Vector3D data[CONCURRENT_INPUTS]; 
} Message;

/** Open response queue */
int openTxQueue(void);

/** Open request queue */
int openRxQueue(void);

/** Close queue */
void closeQueue(mqd_t mqdes);

/** Send results */
void sendResponse(int mqd, Message *message);

/** Receive next request (blocking method)*/
int receiveRequest(int mqd, Message *message);

#endif