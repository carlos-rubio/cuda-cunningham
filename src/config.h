/* 
    CUDA Cunningham - Efficient gravity gradient computation in CUDA enabled GPUs
    Copyright (C) 2024 Carlos Rubio <carlos.rubio@unileon.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CONFIG_H
#define CONFIG_H

/** Maximum gravity expansion degree (and order) */
#define MAX_DEGREE 500

/** Enable logs (comment out to disable logs) */
#define ENABLE_LOG

/**  Double or mixed precision (comment out for the double precision version,
    left uncommented for the mixed precision version) */
#define FLOAT_VERSION

/** Enable double summation version (uncomment for the double summation version) */
//#define DOUBLE_ACCUMULATORS

/** Maximum number of simultaneous gravity computations */
#define CONCURRENT_INPUTS 1

#endif