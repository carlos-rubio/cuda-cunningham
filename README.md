# CUDA-Cunningham

CUDA-Cunningham implements the Cunningham method to compute the geopotential gradient in CUDA-enabled devices. The complete rationale for its implementation is available in: *Efficient computation of the geopotential gradient in graphic processing units*, Advances in Space Research, Volume 74, Issue 1, 2024, Pages 332-347, ISSN 0273-1177, https://doi.org/10.1016/j.asr.2024.04.056
(https://www.sciencedirect.com/science/article/pii/S0273117724004186).

## Download the source code
From a computer with a git client installed, clone the repository by using:

```
git clone https://bitbucket.org/carlos-rubio/cuda-cunningham.git
```

Alternatively you can download the code in a zip file from the downloads page: 

https://bitbucket.org/carlos-rubio/cuda-cunningham/downloads/
 
## Obtain the Stokes coefficients file
At this moment, the unique supported gravity model is the EGM2008. However, you can add other models by changing the *gravityModel.h* and *gravityModel.cu* files. The gravity model filename containing the normalized Stokes coefficients is configured in ***gravityModel.h*** along with its corresponding Earth reference radius and gravitational parameter.
The coefficients file for the EGM2008 can be obtained from the National Geospatial-Intelligence Agency at:

https://earth-info.nga.mil/php/download.php?file=egm-08spherical

## Configure
The configuration is centralized in the file ***config.h*** where it can be configured:

- Maximum gravity degree-order. The implementation uses for the gravity order the same value as configured in the gravity degree. The value needs to be adjusted to the minimum required to minimize the amount of required shared memory per block. 

- Enable or disable logs. Comment the line for maximum performance. If logs are enabled, it prints in the console the request size, the first position and gradient result, and the time used in the computation.

- Double or mixed precision. If commented, it executes all the computations using double-precision arithmetic. If left uncommented, executes all of them using single precision except the initializations and the final gradient summations.

- Double accumulators. Select the most precise but more demanding in shared memory double summation or, if commented, the simple summation scheme.

- Concurrent inputs. The number of simultaneous computations. It also determines the size of the input and output messages.

## Compile
The software is developed and tested only in Linux. Before compile it is needed yo have installed the NVIDIA CUDA toolkit. In Ubuntu can be done by:
```
sudo apt install nvidia-cuda-toolkit
```
Before compile, edit the **_makefile_** and adjust the *CUDAFLAGS* according to the architecture level of the target GPU. The option *-maxrregcount* can also be added to limit the number of registers used by each thread. This is useful only in very specific cases where the number of available registers limits the number of blocks per SM.

Finally, compile to generate the executable by using:

```
make clean; make all
```

## Execute
The executable has two execution modes. Normal execution and autotest. In the normal execution mode, the process opens a POSIX input queue where it receives the computation requests. It also opens another queue to send the responses. The POSIX queue names are configured in the file *middleware.h*.

The messages in the queues have the format:

```
/** Input and output message */
#pragma pack(1)
typedef struct{ 
 int size;
 Vector3D data[CONCURRENT_INPUTS]; 
} Message;
```

With *Vector3D* defined as:
```
/** 3D vector datatype */
#pragma pack(1)
typedef struct {
    double x;
    double y;
    double z;
} Vector3D;
```

As the software is tested in Intel processors, it is assumed by default Little-Endian encoding.

The autotest mode does not open the message queues. Instead, it creates a synthetic request with fixed data and executes it. It is very useful for testing purposes and execution time measurements.

Normal mode is executed by passing the gravity degree-order as a parameter (it should be less or equal to the configured maximum degree). For example, for degree 126:

```
gravity 126
```

The autotest mode is executed when include the *autotest* option at the end:

```
gravity 126 autotest
```

The POSIX queue mechanism is very fast. Therefore, it can be used as an alternative to embedding the CUDA implementation into the user code. The time penalty can be estimated according to:

https://bitbucket.org/carlos-rubio/cuda-cunningham/src/master/images/queueTimes.jpg
